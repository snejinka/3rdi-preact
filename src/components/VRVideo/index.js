import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from '../../style/index.scss';
import Logo from '../../assets/images/logo.png';
import { Entity, Scene } from 'aframe-react';

const Header = () => (
	<Scene class="vr-scene">

		<Entity primitive="a-videosphere" rotation="0 180 0" src="#video" play-on-window-click play-on-vrdisplayactivate-or-enter-vr />
		<Entity primitive="a-assets">
			<video id="video" style="display:none" autoPlay loop crossOrigin="anonymous" playsinline>
				<source type="video/mp4" src="https://ucarecdn.com/01db1006-b954-499e-a784-020951ed5b30/" />
			</video>
		</Entity>
		<Entity primitive="a-camera" look-controls>
			<Entity
				primitive="a-cursor"
				cursor={{ fuse: false }}
				material={{ color: 'white', shader: 'flat', opacity: 0.75 }}
				geometry={{ radiusInner: 0.005, radiusOuter: 0.007 }}
				event-set__1={{
					_event: 'mouseenter',
					scale: { x: 1.4, y: 1.4, z: 1.4 }
				}}
				event-set__2={{
					_event: 'mouseleave',
					scale: { x: 1, y: 1, z: 1 }
				}}
				raycaster="objects: .clickable"
			/>
		</Entity>
	</Scene>

);

export default Header;
