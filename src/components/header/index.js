import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from '../../style/index.scss';
import Logo from '../../assets/images/logo.png';

const Header = () => (
	<header class="header">
		<div class="row justify-content-between align-items-center">
			<div class="col-7 col-md-4 col-lg-2">
				<a class="header__logo" href="/">
					<img class="header__logo" src={Logo} />
				</a>
			</div>
			<div class="d-none d-lg-block col-lg-6 offset-lg-1">
				<ul class="navbar-nav mr-auto header__nav">
					<li class="nav-item">
						<Link class="nav-link header-nav__item" activeclass={style.active}
							  href="/products">Products</Link>
					</li>
					<li class="nav-item">
						<Link class="nav-link header-nav__item" activeclass={style.active}
							  href="/solutions">Solutions</Link>
					</li>
					<li class="nav-item">
						<Link class="nav-link header-nav__item" activeclass={style.active}
							  href="/pricing">Pricing</Link>
					</li>
					<li class="nav-item">
						<Link class="nav-link header-nav__item" activeclass={style.active}
							  href="/company">Company</Link>
					</li>
					<li class="nav-item">
						<Link class="nav-link header-nav__item" activeclass={style.active} href="/blog">Blog</Link>
					</li>
				</ul>
			</div>
			<div class="col-2 col-md-1 col-lg-3">
				<svg viewBox="0 0 24 24" width="24" class="d-none" fill="#ffffff">
					<path d="M0 0h24v24H0z" fill="none"></path>
					<path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
				</svg>
				<div class="row wrap-drop-block wrapDropBlock--3qLcB active" tabIndex="-1">
					<ul class="navbar-nav mr-auto header__nav">
						<li class="nav-item">
							<Link class="nav-link header-nav__item" activeclass={style.active}
								  href="/contact">Contact</Link>
						</li>
						<li class="nav-item">
							<Link class="nav-link header-nav__item" activeclass={style.active} href="/en">En</Link>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>
);

export default Header;
