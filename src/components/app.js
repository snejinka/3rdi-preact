import { h, Component } from 'preact';
import { Router } from 'preact-router';

import Header from './header';
import VRVideo from './VRVideo';
import CenteredBlock from './CenteredBlock';

export default class App extends Component {

	handlePlay = e => {
		this.setState({
			visibleInterface: false
		});
	};
	constructor() {
		super();
		this.state = {
			visibleInterface: true
		};
	}

	render({},{ visibleInterface }) {
		return (
			<div id="app" class="main-container">
				<div class="top-section">
					{visibleInterface &&

					<div className="top-section__interface">
						<div className="container-fluid top-section__container">
							<Header />
							<CenteredBlock handlePlay={this.handlePlay} />
							<div className="row">
								<div className="col">
									author
								</div>
								<div className="col">qr</div>
							</div>

						</div>
					</div>
					}

					<VRVideo />
				</div>
				<div class="content-section">
					<h1>hello world!</h1>
				</div>
			</div>
		);
	}
}
