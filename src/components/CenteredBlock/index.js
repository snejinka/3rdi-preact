import { Component, h } from 'preact';
import { Link } from 'preact-router/match';
import style from '../../style/index.scss';

class CenteredBlock extends Component{

	constructor() {
		super();
		this.state = {
		};
	}

	render({ handlePlay },{}) {
		return (
			<div class="centered-block">
				<div class="row justify-content-between align-items-center">
					<div class="col text-center">
						<h1>Feel and discover</h1>
						<h2>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</h2>
						<button onClick={handlePlay}>Press to rotate</button>
					</div>
				</div>
			</div>


		);
	}
}

export default CenteredBlock;
