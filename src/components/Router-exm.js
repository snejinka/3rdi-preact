import Home from './app';
import { Router } from 'preact-router';
import { Link } from 'preact-router/match';


handleRoute = e => {
	this.currentUrl = e.url;
};
{/*<Router onChange={this.handleRoute}>*/}
	{/*<Home path="/" />*/}
	{/*<Profile path="/profile/" user="me" />*/}
	{/*<Profile path="/profile/:user" />*/}
{/*</Router>*/}


<header className={style.header}>
	<h1>Preact App</h1>
	<nav>
		<Link activeClassName={style.active} href="/">Home</Link>
		<Link activeClassName={style.active} href="/profile">Me</Link>
		<Link activeClassName={style.active} href="/profile/john">John</Link>
	</nav>
</header>