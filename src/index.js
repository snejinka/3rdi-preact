import './style';
import App from './components/app';
import 'aframe';
import 'aframe-animation-component';
import 'aframe-event-set-component';
import 'aframe-particle-system-component';
import 'bootstrap/dist/css/bootstrap.min.css';

export default App;
